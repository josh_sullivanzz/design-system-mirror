export { default as MediaTable } from './component/mediaTable';

export { MediaTableProps, MediaTableItem, SortOrderType } from './types';
