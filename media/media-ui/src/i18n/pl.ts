/**
 * NOTE:
 *
 * This file is automatically generated by i18n-tools.
 * DO NOT CHANGE IT BY HAND or your changes will be lost.
 */
// Polish
export default {
  'fabric.media.accounts': 'Konta',
  'fabric.media.actions': 'Akcje',
  'fabric.media.add_account': 'Dodaj konto',
  'fabric.media.annotate': 'Dodaj adnotację',
  'fabric.media.annotate.confirmation.close.anyway': 'Zamknij mimo to',
  'fabric.media.annotate.confirmation.content':
    'Masz niezapisane zmiany. Czy na pewno chcesz wyjść?',
  'fabric.media.annotate.confirmation.heading': 'Niezapisane zmiany',
  'fabric.media.annotate.tool.arrow': 'Strzałka',
  'fabric.media.annotate.tool.blur': 'Rozmycie',
  'fabric.media.annotate.tool.brush': 'Pędzel',
  'fabric.media.annotate.tool.color': 'Kolor',
  'fabric.media.annotate.tool.line.thickness': 'Grubość linii',
  'fabric.media.annotate.tool.shape': 'Kształt',
  'fabric.media.annotate.tool.text': 'Tekst',
  'fabric.media.audio': 'dźwięk',
  'fabric.media.cancel': 'Anuluj',
  'fabric.media.cant_preview_file_type':
    'Podgląd plików tego typu nie jest możliwy.',
  'fabric.media.cant_retrieve_files': 'Ojej! Nie można pobrać żadnych plików',
  'fabric.media.cant_retrieve_gifs':
    'Ojej! Nie można pobrać żadnych plików GIF',
  'fabric.media.check_your_network': 'Sprawdź swoje połączenie sieciowe.',
  'fabric.media.close': 'Zamknij',
  'fabric.media.connect_account_description':
    'Otworzymy nową stronę, aby pomóc Ci połączyć swoje konto {name}',
  'fabric.media.connect_link_account': 'Połącz podgląd',
  'fabric.media.connect_link_account_card_view': 'Połącz',
  'fabric.media.connect_link_account_card_view_description':
    'Łącząc {context} z produktami Atlassian, uatrakcyjnisz podgląd łączy.',
  'fabric.media.connect_link_account_card_view_name': 'Połącz konto {context}',
  'fabric.media.connect_to': 'Połącz z {name}',
  'fabric.media.could_not_load_editor': 'Oj! Nie można załadować edytora',
  'fabric.media.could_not_save_image': 'Oj! Nie można zapisać obrazu',
  'fabric.media.couldnt_generate_preview':
    'Nie można wygenerować podglądu tego pliku.',
  'fabric.media.couldnt_load_link':
    'Z jakiegoś powodu nie udało się wczytać tego łącza.',
  'fabric.media.default_avatars': 'Domyślne awatary',
  'fabric.media.disable_fullscreen': 'wyłącz tryb pełnoekranowy',
  'fabric.media.document': 'dokument',
  'fabric.media.download': 'Pobierz',
  'fabric.media.drag_and_drop_images_here': 'Przeciągnij i upuść obrazy tutaj',
  'fabric.media.drag_and_drop_your_files':
    'Przeciągnij i upuść pliki w dowolne miejsce lub',
  'fabric.media.drop_your_files': 'Upuść pliki, aby je przekazać',
  'fabric.media.drop_your_files_here': 'Upuść pliki tutaj',
  'fabric.media.enable_fullscreen': 'włącz tryb pełnoekranowy',
  'fabric.media.error_generating_preview': 'Błąd generowania podglądu',
  'fabric.media.error_hint_critical':
    'Jeśli problem nadal będzie występować, skontaktuj się ze wsparciem.',
  'fabric.media.error_hint_retry': 'Spróbuj ponownie.',
  'fabric.media.error_loading_file': 'Błąd ładowania pliku',
  'fabric.media.failed_to_load': 'Niepowodzenie ładowania',
  'fabric.media.give_feedback': 'Prześlij opinię',
  'fabric.media.image': 'obraz',
  'fabric.media.image.search': 'Szukaj',
  'fabric.media.image_format_invalid_error':
    'Nie można załadować obrazu. Format jest niepoprawny.',
  'fabric.media.image_size_too_large_error':
    'Obraz jest za duży,  rozmiar nie może przekraczać {MAX_SIZE_MB} MB',
  'fabric.media.image_url_invalid_error':
    'Nie można załadować obrazu. Adres URL jest niepoprawny.',
  'fabric.media.insert_files':
    '{0, plural, one {Wstaw {0} plik} few {Wstaw {0} pliki} many {Wstaw {0} plików} other {Wstaw {0} pliku}}',
  'fabric.media.invalid_permissions': 'Nie masz dostępu do tego łącza',
  'fabric.media.invalid_permissions_description':
    'Musisz poprosić o dostęp lub spróbować innego konta, aby zobaczyć ten podgląd.',
  'fabric.media.item_not_found_in_list':
    'Zaznaczony element nie został odnaleziony na liście.',
  'fabric.media.learn_more': 'Dowiedz się więcej',
  'fabric.media.load_more_gifs': 'Załaduj więcej plików GIF',
  'fabric.media.loading': 'Wczytywanie...',
  'fabric.media.might_be_a_hiccup': 'To może być przejściowy problem.',
  'fabric.media.no_gifs_found': 'Halo! Czy to mnie szukasz?',
  'fabric.media.no_gifs_found_suggestion':
    'Nie można znaleźć wyników pasujących do zapytania „{query}”',
  'fabric.media.no_pdf_artifacts': 'Nie znaleziono artefaktów PDF w tym pliku.',
  'fabric.media.not_found_description':
    'Sprawdź adres url i spróbuj go edytować lub wkleić ponownie.',
  'fabric.media.not_found_title': 'Niestety. Nie można znaleźć tego łącza!',
  'fabric.media.or': 'lub',
  'fabric.media.pause': 'wstrzymaj',
  'fabric.media.play': 'odtwórz',
  'fabric.media.recent_uploads': 'Ostatnio przekazane',
  'fabric.media.remove_image': 'usuń obraz',
  'fabric.media.retry': 'Spróbuj ponownie',
  'fabric.media.save': 'Zapisz',
  'fabric.media.search_all_gifs': 'Szukaj wszystkich plików GIF!',
  'fabric.media.share_files_instantly': 'Błyskawicznie je udostępnimy',
  'fabric.media.something_went_wrong': 'Coś poszło nie tak.',
  'fabric.media.srclink': 'Wyświetl w',
  'fabric.media.srclinkunknown': 'Wyświetl oryginał',
  'fabric.media.try_again': 'Spróbuj ponownie',
  'fabric.media.try_another_account': 'Spróbuj użyć innego konta',
  'fabric.media.try_downloading_file':
    'Spróbuj pobrać plik, aby go wyświetlić.',
  'fabric.media.unable_to_annotate_image':
    'Nie można dodać adnotacji do tego obrazu',
  'fabric.media.unknown': 'nieznane',
  'fabric.media.unlink_account': 'Odłącz konto',
  'fabric.media.upload': 'Przekaż',
  'fabric.media.upload_an_avatar': 'Przekaż awatar',
  'fabric.media.upload_file': 'Przekaż plik',
  'fabric.media.upload_file_from': 'Przekaż plik z {name}',
  'fabric.media.upload_image': 'Przekaż obraz',
  'fabric.media.upload_photo': 'Przekaż zdjęcie',
  'fabric.media.video': 'wideo',
  'fabric.media.webgl_warning_description':
    'Twoja przeglądarka nie obsługuje WebGL. Użyj przeglądarki z włączoną obsługą WebGL, aby dodawać adnotacje do obrazów',
  'fabric.media.zoom_in': 'powiększ',
  'fabric.media.zoom_out': 'pomniejsz',
};
