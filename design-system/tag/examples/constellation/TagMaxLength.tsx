import React from 'react';
import Tag from '../../src';

const cupcakeipsum = 'Croissant tiramisu gummi bears.';

export default () => <Tag text={cupcakeipsum} />;
