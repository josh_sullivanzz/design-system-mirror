import JiraServiceDeskIcon from './Icon';
import JiraServiceDeskLogo from './Logo';
import JiraServiceDeskWordmark from './Wordmark';

export { JiraServiceDeskLogo, JiraServiceDeskIcon, JiraServiceDeskWordmark };
