import React from 'react';

import { colors } from '@atlaskit/theme';

import { Skeleton } from '../../src';

export default function AvatarSkeletonWeightStrongExample() {
  return <Skeleton color={colors.Y500} weight="strong" />;
}
