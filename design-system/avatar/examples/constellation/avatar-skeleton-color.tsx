import React from 'react';

import { colors } from '@atlaskit/theme';

import { Skeleton } from '../../src';

export default function AvatarSkeletonColorExample() {
  return <Skeleton color={colors.B300} />;
}
