import React from 'react';

import Avatar from '../../src';

export default function AvatarSmallExample() {
  return (
    <div>
      <Avatar size="small" />
      <Avatar size="small" appearance="square" />
    </div>
  );
}
