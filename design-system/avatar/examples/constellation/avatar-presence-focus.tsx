import React from 'react';

import { Presence } from '../../src';

export default function AvatarPresenceFocusExample() {
  return (
    <div style={{ width: 24 }}>
      <Presence presence="focus" />
    </div>
  );
}
