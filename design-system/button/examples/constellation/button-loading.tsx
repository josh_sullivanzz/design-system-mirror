import React from 'react';

import Button from '../../src';

export default () => (
  <Button appearance="primary" isLoading>
    Loading button
  </Button>
);
