import React from 'react';

import Button from '../../src';

export default () => <Button appearance="subtle">Subtle button</Button>;
