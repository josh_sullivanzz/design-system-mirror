# Flag

A flag is used for confirmations, alerts, and acknowledgments that require minimal user interaction.

## Installation

```sh
yarn add @atlaskit/flag
```

## Usage

Detailed docs and example usage can be found [here](https://atlassian.design/components/flag/).
