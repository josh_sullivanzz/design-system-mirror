import React from 'react';

import Badge from '../../src';

export default function Example() {
  return <Badge appearance="important">{25}</Badge>;
}
