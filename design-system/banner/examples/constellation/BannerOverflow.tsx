import React from 'react';

import WarningIcon from '@atlaskit/icon/glyph/warning';

import Banner from '../../src';

const message: string =
  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum lobortis, odio egestas pulvinar sodales, neque justo tempor tellus, eget venenatis arcu ante non purus. Pellentesque tellus eros, rutrum vel enim non, tempor faucibus felis. Nullam pharetra erat sed magna porttitor, eget tincidunt odio finibus';

export default () => (
  <div style={{ maxWidth: 400, margin: 'auto' }}>
    <Banner
      icon={<WarningIcon label="Warning icon" secondaryColor="inherit" />}
      isOpen
    >
      {message}
    </Banner>
  </div>
);
