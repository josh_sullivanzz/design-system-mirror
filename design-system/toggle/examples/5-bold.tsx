import React from 'react';

import Toggle from '../src';

export default () => (
  <div>
    <Toggle size="large" />
    <Toggle size="large" isDefaultChecked />
  </div>
);
