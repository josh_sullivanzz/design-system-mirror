import React from 'react';

import InlineMessage from '../../src';

export default () => (
  <InlineMessage placement="right" title="Title" secondaryText="Secondary text">
    <p>Dialog to the right</p>
  </InlineMessage>
);
