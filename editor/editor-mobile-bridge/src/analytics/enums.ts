export enum ActionSubject {
  EDITOR = 'editor',
}

export enum ActionSubjectID {
  COLLAB = 'collab',
}

export enum EventType {
  TRACK = 'track',
}
