import { version } from './version.json';
import { DocumentReflowDetector } from './document-reflow-detector';

type Padding = { top: number; right: number; bottom: number; left: number };

export const defaultPadding = [16, 16, 0, 16];

export default abstract class WebBridge {
  private reflowDetector = new DocumentReflowDetector({
    onReflow: this.sendHeight,
  });

  constructor() {
    // Set initial page padding (necessary for seeing the gap cursor for some content nodes).
    // This may be overwritten at runtime by a native bridge consumer.
    this.setPadding(...defaultPadding);
  }

  private padding: Padding = { top: 0, right: 0, bottom: 0, left: 0 };

  abstract getRootElement(): HTMLElement | null;
  abstract sendHeight(height: number): void;

  setPadding(
    top: number = 0,
    right: number = 0,
    bottom: number = 0,
    left: number = 0,
  ) {
    let root = this.getRootElement();
    if (root) {
      // bottom introduces a non-clickable area, so remove it
      root.style.padding = `${top}px ${right}px 0px ${left}px`;
      this.padding = { top, right, bottom: 0, left };
    }
  }

  getPadding(): Padding {
    return this.padding;
  }

  /**
   * Used to observe the height of the rendered content and notify the native side when that happens
   * by calling RenderBridge#onRenderedContentHeightChanged.
   *
   * @param enabled whether the height is being observed (and therefore the callback is being called).
   */
  observeRenderedContentHeight(enabled: boolean) {
    if (enabled) {
      this.reflowDetector.enable();
    } else {
      this.reflowDetector.disable();
    }
  }

  reload(): void {
    window.location.reload();
  }

  currentVersion(): string {
    return version;
  }
}
