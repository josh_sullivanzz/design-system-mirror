export default [
  'hardBreak',
  'mention',
  'emoji',
  'inlineExtension',
  'date',
  'placeholder',
  'inlineCard',
  'status',
  'formatted_text_inline',
  'code_inline',
];
